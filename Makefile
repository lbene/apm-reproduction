SHELL := /bin/bash

grn=$'\e[1;32m
yel=$'\e[1;33m
end=$'\e[0m

-include .env

configure-env:
	@printf "$(grn)\nConfiguring environment variables..$(end)\n"
	./bin/configure_env.sh

# Docker operations.
start:
	make configure-env
	if [ -f "docker-compose.override.yaml" ]; then \
		docker-compose -f docker-compose.yml -f docker-compose.override.yaml up -d ;\
	else \
		docker-compose up -d ;\
  	fi

stop:
	docker-compose down ;\

restart:
	docker-compose down ;\
	make start

list:
	docker-compose ps

docker-update: stop
	docker-compose pull ;\
	docker volume prune -f || true ;\
	docker-compose up -d --build ;\

docker-clean:
	docker ps -qa --no-trunc --filter "status=exited" | xargs docker rm || true ;\
	docker images -f "dangling=true" -q | xargs docker rmi || true ;\
	docker volume prune -f || true ;\

ssh:
	docker-compose exec php bash

# Xdebug causes performance issues, disable it for commands,migrations etc.
disable-xdebug:
	if [ "$(CI)" == "true" ]; then \
  		dxd || true ;\
	else \
		docker-compose exec -T php dxd || true ;\
	fi

enable-xdebug:
	if [ "$(CI)" == "true" ]; then \
		mv /usr/local/etc/php/conf.d/disabled/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini ;\
	else \
		docker-compose exec -T php exd || true ;\
	fi
# End of - Docker operations.

# Database operations.
migrate: disable-xdebug
	@printf "$(grn)\nMigrating schema..$(end)\n"
	@make migrate-schema
	@printf "$(grn)\nMigrating data..$(end)\n"
	@make migrate-data
	@if [ "$(ENV)" == "local" ]; then \
		printf "$(grn)\nMigrating local only data..$(end)\n" ;\
  		make migrate-data-local ;\
	fi

# Application operations.
sf:
	docker-compose exec php bin/console $(filter-out $@,$(MAKECMDGOALS))

composer: disable-xdebug
	docker-compose exec php composer $(filter-out $@,$(MAKECMDGOALS))

clear-cache: disable-xdebug
	docker-compose exec php bin/console c:c ;\
	docker-compose exec php bin/console cache:pool:clear cache.app ;\

clear-cache-test: disable-xdebug
	docker-compose exec php bin/console c:c --env test;\

reset-cache-for-test:
	make clear-cache && make clear-cache-test && make enable-xdebug

deploy:
	make start
	make composer install
	make initialize-db

update-app: disable-xdebug
	make start
	make composer install
	make migrate

open-local:
	@printf "$(grn)\nOpening local website..$(end)\n"
	which xdg-open || exit 1
	xdg-open ${APP_URL}

audit:
	docker-compose exec php symfony security:check ;\

push-no-check:
	NO_PHPQA=true git push origin HEAD

force-push-no-check:
	NO_PHPQA=true git push origin HEAD -f

# Required to allow make command parameters.
%:
	@:
