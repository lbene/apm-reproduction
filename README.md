# APM Reproduction Repository for Site24x7

## Installation

### Requirements

- Ubuntu Linux
- GNU Make 4.2.1
- Docker version 20.10.22
- docker-compose version 1.29.2
- ifconfig command from net-tools package

### Installation steps

1. `make start`
2. `make composer i`
3. Add `0.0.0.0 local.php-with-apm.com` to `/etc/hosts`
4. `make list`
5. Navigate to `http://local.php-with-apm.com:8091/` or `https://local.php-with-apm.com:8092/`
